package main

import (
	"flag"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"text/template"
)

const (
	ERROR_INVALID_PARAMETER       int = 1
	ERROR_READING_FILE            int = 2
	ERROR_UNMARSHALLING_YAML      int = 3
	ERROR_PARSING_POLICY_TEMPLATE int = 4
	ERROR_GENERATING_POLICY       int = 5
)

type AwsUsers struct {
	Users [] AwsUser
}

type AwsUser struct {
	Userarn   string   `yaml:"userarn"`
	Username  string   `yaml:"username"`
	Namespace string   `yaml:"namespace"`
	Groups    []string `yaml:"groups"`
}

var userList, policyTemplate, policyOutputFile string

const defaultUsers string = "users.yaml" // Default name of the user list files
const defaultPolicyTemplate string = "tpolicy.yaml"
const defaultOutputPolicyFile string = "policy.yaml"

func main() {
	log.Printf("The list of users is being extracted from '%s'\n", userList)

	users := getUsers(userList)
	generatePolicyGroup(policyTemplate, policyOutputFile, users)

	for e := range users.Users {
		log.Printf("Generating policy for Aws User : %v\n", users.Users[e])
		//generatePolicy(policyTemplate, policyOutputFile, users[e])
	}

	log.Printf("Finished generating policy files for all AWS users")
}

func init() {
	flag.StringVar(&userList,
		"user-list",
		defaultUsers,
		"YAML file containing the lists of users who have to be added to Kubernetes")
	flag.StringVar(&policyTemplate,
		"policy-template",
		defaultPolicyTemplate,
		"YAML file containing the lists of users who have to be added to Kubernetes")
	flag.StringVar(&policyOutputFile,
		"output-policy-file",
		defaultOutputPolicyFile,
		"Output directory to write processed policy files")
	flag.Parse()
	if (userList == "" || policyTemplate == "") {
		flag.Usage()
		os.Exit(ERROR_INVALID_PARAMETER)
	}
}

func getUsers(usersFile string) AwsUsers {
	// Read the list of users
	source, err := ioutil.ReadFile(usersFile)
	if err != nil {
		log.Fatalf("Fatal error reading user data file: %s \n", err)
		os.Exit(ERROR_READING_FILE)
	}

	var awsUsers AwsUsers
	// Unmarshal the aws users from the data file
	err = yaml.Unmarshal(source, &awsUsers)
	if err != nil {
		log.Fatalf("Unable to decode into struct, %v\n", err)
		os.Exit(ERROR_UNMARSHALLING_YAML)
	}
	return awsUsers
}

func generatePolicyGroup(policyTemplate string, output string, users AwsUsers) {
	// Process the template
	tmpl, err := template.
		ParseFiles(policyTemplate)
	if err != nil {
		log.Fatalf("Error parsing template : %v", err)
		os.Exit(ERROR_PARSING_POLICY_TEMPLATE)
	}
	// Create temporary file to output the generated policy
	f, err := os.Create(output)
	if err != nil {
		log.Fatalf("Error creating files to output generated policy file %v", err)
		os.Exit(ERROR_GENERATING_POLICY)
	}
	// Generate the policy from the template
	err = tmpl.Execute(f, users)
	if err != nil {
		log.Fatalf("Error creating files to output generated policy file %v", err)
		os.Exit(ERROR_GENERATING_POLICY)
	}
	f.Close()
}
